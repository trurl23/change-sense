package net.trurl.changesense.exception;

public class ChangeSenseArchiveException extends ChangeSenseException {

    public ChangeSenseArchiveException(final String s) {
        super(s);
    }

    public ChangeSenseArchiveException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

    public ChangeSenseArchiveException(final Throwable throwable) {
        super(throwable);
    }
}
