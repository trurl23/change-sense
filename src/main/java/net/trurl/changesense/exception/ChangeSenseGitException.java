package net.trurl.changesense.exception;

public class ChangeSenseGitException extends ChangeSenseException {

    public ChangeSenseGitException(final String s) {
        super(s);
    }

    public ChangeSenseGitException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

    public ChangeSenseGitException(final Throwable throwable) {
        super(throwable);
    }
}
