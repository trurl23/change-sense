package net.trurl.changesense.exception;

public class ChangeSensePropertiesException extends ChangeSenseException {

    public ChangeSensePropertiesException(final String s) {
        super(s);
    }

    public ChangeSensePropertiesException(final String s, final Throwable throwable) {
        super(s, throwable);
    }

    public ChangeSensePropertiesException(final Throwable throwable) {
        super(throwable);
    }
}
