package net.trurl.changesense.model;

import net.trurl.changesense.service.RepositoryService;
import net.trurl.changesense.service.parser.PropertyParser;

/**
 * Configuration of change-sense library.
 */
public interface ChangeSenseConfiguration {
    /**
     * If set, use this number of bytes as maximum size when fetching a file from the repo. If not specified,
     * defaults to 1MiB. Must be at least 1KiB.
     *
     * @return maximum size of files in bytes.
     */
    int getMaximumSizeOfFilesBytes();

    /**
     * Return class to use for parsing files with the given extension.
     *
     * @param parserId Parser ID.
     * @return Class of property parser or null, if no parser with the given id has been registered.
     */
    Class<? extends PropertyParser> getParser(String parserId);

    /**
     * Return class to use for default repository service implementation.
     *
     * @return repository service implementation.
     */
    Class<? extends RepositoryService> getRepositoryServiceClass();
}
