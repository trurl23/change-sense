package net.trurl.changesense.model;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Stream;

/**
 * This class represents a single changed file. Flags denote the change type. More than one flag can be present.
 */
public interface ChangedFile {

    /**
     * Type of file change.
     */
    enum ChangeType {
        /**
         * File has been added.
         */
        ADDED(1),
        /**
         * File has been removed.
         */
        REMOVED(2),
        /**
         * File contents have been changed.
         */
        CHANGED(4),
        /**
         * File has been moved or renamed.
         */
        MOVED(8),
        /**
         * File has been copied.
         */
        COPIED(16),
        /**
         * File mode was changed.
         * TODO support me
         */
        MODE(32);

        private final int type;

        /**
         * Create a change type enum value.
         *
         * @param type a bitmask for the enum value.
         */
        ChangeType(final int type) {
            this.type = type;
        }

        /**
         * Get bitmask of this enum value.
         *
         * @return a bitmask.
         */
        public int getType() {
            return type;
        }

        /**
         * Get enum list from the given flags.
         *
         * @param flags Flags.
         * @return List of enumeration values corresponding to the flags.
         */
        public static List<ChangeType> fromFlags(final int flags) {
            final List<ChangeType> result = new LinkedList<>();
            Stream.of(values())
                    .forEach(value -> {
                        if ((flags & value.getType()) != 0) {
                            result.add(value);
                        }
                    });
            return result;
        }

        /**
         * Get combination of flags from list.
         *
         * @param changeTypes List of change types.
         * @return combination of flags.
         */
        public static int fromList(final List<ChangeType> changeTypes) {
            int result = 0;
            for (final ChangeType changeType : changeTypes) {
                result |= changeType.getType();
            }
            return result;
        }
    }

    /**
     * Return file name in the repo.
     *
     * @return File name in the repository.
     */
    String getFilename();

    /**
     * Get change type.
     *
     * @return a combination of {@link ChangeType} flags.
     */
    int getChangeType();

    /**
     * Check if changed file has a certain change type.
     *
     * @param changeType Change type to check.
     * @return true if the change type is present, false otherwise.
     */
    boolean is(ChangeType... changeType);

    /**
     * Fetch old revision of changed file.
     *
     * @return contents of the file up to {@link ChangeSenseConfiguration#getMaximumSizeOfFilesBytes()}.
     */
    byte[] fetchOldRevision();

    /**
     * Fetch new revision of changed file.
     *
     * @return contents of the file up to {@link ChangeSenseConfiguration#getMaximumSizeOfFilesBytes()}.
     */
    byte[] fetchNewRevision();

}
