package net.trurl.changesense.model;

/**
 * An object that holds a reference to the main configuration.
 */
public interface Configurable {
    /**
     * Get current system configuration.
     *
     * @return configuration object.
     */
    ChangeSenseConfiguration getConfig();
}
