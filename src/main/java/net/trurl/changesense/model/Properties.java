package net.trurl.changesense.model;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * A list of properties along with their documentation.
 */
public interface Properties {
    /**
     * Get list of properties.
     *
     * @return a list of properties.
     */
    List<Property> getProperties();

    /**
     * Checks if the given property is present.
     *
     * @param name Name of property.
     * @return true if the property is in the property list.
     */
    boolean hasProperty(final String name);

    /**
     * Get property by name.
     *
     * @param name Name of property.
     * @return the associated property instance or null if the property was not in the list.
     */
    Property getProperty(final String name);

    /**
     * Read properties from the supplied input stream.
     *
     * @param inputStream Stream to read the properties from.
     * @param parserId    ID of parser to use.
     * @return true, if the properties were read successfully, false if there was no parser with the given ID.
     * @throws IOException when the input stream could not be read.
     */
    boolean readFrom(InputStream inputStream, String parserId) throws IOException;
}
