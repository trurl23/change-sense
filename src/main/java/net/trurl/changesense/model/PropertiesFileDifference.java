package net.trurl.changesense.model;

import java.util.List;

/**
 * Encapsulates a list of changed properties of a single properties file.
 */
public interface PropertiesFileDifference {
    /**
     * Get the underlying changed file.
     *
     * @return an instance of {@link ChangedFile}.
     */
    ChangedFile getFile();

    /**
     * Get list of all property differences.
     *
     * @return a list of all differences.
     */
    List<PropertyDifference> getProperties();

    /**
     * Get a list of property differences of certain types.
     *
     * @param differenceTypes A collection of difference types.
     * @return a list of property differences matching any of the specified types.
     */
    List<PropertyDifference> getProperties(PropertyDifference.DifferenceType... differenceTypes);
}
