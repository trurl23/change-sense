package net.trurl.changesense.model;

/**
 * One property along with documentation.
 */
public interface Property {
    /**
     * Property name.
     *
     * @return name of property.
     */
    String getName();

    /**
     * Property value.
     *
     * @return value of property.
     */
    String getValue();

    /**
     * Property documentation.
     *
     * @return documentation of property.
     */
    String getDocumentation();
}
