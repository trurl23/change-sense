package net.trurl.changesense.model;

/**
 * Base interface for repository configuration objects. In general, you can obtain a matching {@link ChangeRepository}
 * instance by passing an instance implementing this interface to
 * {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)}.
 */
public interface RepositoryConfiguration {
}
