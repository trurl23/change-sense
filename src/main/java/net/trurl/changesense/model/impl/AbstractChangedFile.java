package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangedFile;

import java.util.Arrays;

/**
 * Abstract base class for all types of {@link ChangedFile}.
 */
public abstract class AbstractChangedFile implements ChangedFile {
    /**
     * Change repository.
     */
    private final ChangeRepository changeRepository;

    protected AbstractChangedFile(final ChangeRepository changeRepository) {
        this.changeRepository = changeRepository;
    }

    @Override
    public byte[] fetchOldRevision() {
        return changeRepository.fetchOldRevision(this);
    }

    @Override
    public byte[] fetchNewRevision() {
        return changeRepository.fetchNewRevision(this);
    }

    @Override
    public boolean is(final ChangeType... changeTypes) {
        final int flags = ChangeType.fromList(Arrays.asList(changeTypes));
        return (getChangeType() & flags) != 0;
    }
}
