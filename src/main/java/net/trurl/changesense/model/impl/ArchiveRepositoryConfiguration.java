package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.RepositoryConfiguration;

/**
 * Configuration values of {@link ArchiveChangeRepository}.
 * <p>
 * Use an instance of this configuration class to obtain an instance of a {@link GitChangeRepository} via a call to
 * {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} on the
 * {@link DefaultRepositoryService} which you in turn can get via
 * {@link net.trurl.changesense.service.RepositoryService#createDefaultRepositoryService()}.
 */
public class ArchiveRepositoryConfiguration implements RepositoryConfiguration {

}
