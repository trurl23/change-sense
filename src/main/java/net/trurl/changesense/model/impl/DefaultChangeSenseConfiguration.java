package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.service.RepositoryService;
import net.trurl.changesense.service.parser.PropertyParser;
import net.trurl.changesense.service.parser.v1.DefaultPropertyParser;

import java.util.HashMap;
import java.util.Map;

/**
 * Default configuration, registers {@link DefaultPropertyParser} as "properties".
 * Can be used to easily set up a suitable configuration.
 */
public class DefaultChangeSenseConfiguration implements ChangeSenseConfiguration {

    /**
     * Mapping of file extensions (without the dot) to custom parsers.
     */
    private final Map<String, Class<? extends PropertyParser>> parserMap = new HashMap<>();

    /**
     * Mapping of file extensions (without the dot) to default parsers.
     */
    private final Map<String, Class<? extends PropertyParser>> defaultParsers = new HashMap<>();

    public DefaultChangeSenseConfiguration() {
        defaultParsers.put("properties", DefaultPropertyParser.class);
    }

    @Override
    public int getMaximumSizeOfFilesBytes() {
        return 1024 * 1024;
    }

    /**
     * Register custom parser for extension.
     *
     * @param extension File extension without the dot.
     * @param clazz     Property parser class to use.
     */
    public void registerParser(final String extension, final Class<? extends PropertyParser> clazz) {
        parserMap.put(extension, clazz);
    }

    @Override
    public Class<? extends PropertyParser> getParser(final String parserId) {
        if (!parserMap.containsKey(parserId)) {
            return defaultParsers.get(parserId);
        }
        return parserMap.get(parserId);
    }

    @Override
    public Class<? extends RepositoryService> getRepositoryServiceClass() {
        return DefaultRepositoryService.class;
    }
}
