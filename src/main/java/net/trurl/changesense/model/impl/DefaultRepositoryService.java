package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.service.RepositoryService;
import net.trurl.changesense.service.impl.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Collectors;

/**
 * Default repository service implementation. Delegates leg work to concrete implementation classes.
 * Auto-detects if JGit or commons-compress is available in the class path to enable the matching services.
 * <p>
 * Call {@link #createChangeRepository(RepositoryConfiguration)} with one of the implementations of
 * {@link RepositoryConfiguration} which are supported by this implementation:
 * <ul>
 * <li>{@link GitRepositoryConfiguration} - work with local git repository</li>
 * <li>{@link GitRemoteRepositoryConfiguration} - work with remote git repository</li>
 * <li>{@link FilesystemRepositoryConfiguration} - work with directories</li>
 * <li>{@link ArchiveRepositoryConfiguration} - work with archives</li>
 * </ul>
 */
public class DefaultRepositoryService extends DelegatingRepositoryService {

    private static final Logger logger = LoggerFactory.getLogger(DefaultRepositoryService.class);

    /**
     * Create new instance, detect supported repository services and log status.
     *
     * @param changeSenseConfiguration Change sense configuration to use.
     */
    public DefaultRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        super(changeSenseConfiguration);
        detectServices(changeSenseConfiguration);
        logStatus();
    }

    private void logStatus() {
        logger.info("Default repository service supports in the following order: {}",
                getDelegates().stream().map(RepositoryService::getName).collect(Collectors.joining(", ")));
    }

    private void detectServices(final ChangeSenseConfiguration changeSenseConfiguration) {
        if (isJGitDetected()) {
            registerDelegate(new GitRemoteRepositoryService(changeSenseConfiguration));
            registerDelegate(new GitRepositoryService(changeSenseConfiguration));
        }
        registerDelegate(new FilesystemChangeRepositoryService(changeSenseConfiguration));
        if (isCommonsCompressDetected()) {
            registerDelegate(new ArchiveChangeRepositoryService(changeSenseConfiguration));
        }
    }

    private boolean isCommonsCompressDetected() {
        return classExists("org.apache.commons.compress.archivers.ArchiveInputStream");
    }

    private boolean isJGitDetected() {
        return classExists("org.eclipse.jgit.api.Git");
    }

    private boolean classExists(final String canonicalName) {
        try {
            Class.forName(canonicalName);
        } catch (final ClassNotFoundException e) {
            return false;
        }
        return true;
    }


}
