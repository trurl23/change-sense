package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.ChangedFile;
import net.trurl.changesense.model.RepositoryConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.apache.commons.io.FileUtils.byteCountToDisplaySize;

/**
 * Implementation of a {@link ChangeRepository} that works with the file system.
 * <p>
 * Use {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} with a
 * {@link FilesystemRepositoryConfiguration} object to obtain an instance of this repository.
 * <p>
 * Unlike versioning system repositories, directory trees are used as "revisions" by this implementation. To compare
 * two revisions, just pass the absolute paths of two directories in the revision strings to
 * {@link #calcChangedFiles(String, String, Pattern)}. The repository will then calculate the differences between the
 * two directory trees.
 * <p>
 * For sanity, a maximum of {@value #MAX_NESTED_DIRECTORIES} nested directories is supported.
 */
public class FilesystemChangeRepository implements ChangeRepository {

    /**
     * Maximum number of supported nested directories.
     */
    private static final int MAX_NESTED_DIRECTORIES = 20;

    /**
     * System configuration.
     */
    private final ChangeSenseConfiguration changeSenseConfiguration;

    /**
     * Create filesystem change repository.
     *
     * @param changeSenseConfiguration System configuration object.
     */
    public FilesystemChangeRepository(final ChangeSenseConfiguration changeSenseConfiguration) {
        this.changeSenseConfiguration = changeSenseConfiguration;
    }

    @Override
    public List<ChangedFile> calcChangedFiles(final String revisionA, final String revisionB,
                                              final Pattern filenamePattern) {
        final File revA = resolveReference(revisionA);
        final File revB = resolveReference(revisionB);
        return determineChangedFiles(revA, revB, filenamePattern);
    }

    @Override
    public boolean tryResolveReference(final String revision) {
        final File revFile = new File(revision);
        return revFile.exists() && revFile.isDirectory() && revFile.canRead();
    }

    /**
     * Resolve reference (ensure that revision exists, is a directory and can be read).
     *
     * @param revision Absolute path to revision directory.
     * @return a file instance pointing to the revision directory.
     */
    private File resolveReference(final String revision) {
        final File revFile = new File(revision);
        if (!revFile.exists() || !revFile.isDirectory() || !revFile.canRead()) {
            throw new IllegalArgumentException(String.format(
                    "'%s' does not exist, is not readable or is not a directory!", revision));
        }
        return revFile;
    }

    @Override
    public byte[] fetchOldRevision(final ChangedFile changedFile) {
        final FilesystemChangedFile file = checkAndCast(changedFile);
        final File fileToRead = file.getOriginalRevisionFile();
        return readFile(fileToRead);
    }

    /**
     * Read file to byte array (if it does not exceed maximum allowed size).
     *
     * @param fileToRead File to read.
     * @return byte array containing file data.
     */
    private byte[] readFile(final File fileToRead) {
        if (fileToRead.length() < changeSenseConfiguration.getMaximumSizeOfFilesBytes()) {
            try {
                return FileUtils.readFileToByteArray(fileToRead);
            } catch (final IOException e) {
                throw new UncheckedIOException(e);
            }
        } else {
            throw new IllegalStateException(String.format("'%s' exceeds maximum file size (%s < %s)!",
                    fileToRead,
                    byteCountToDisplaySize(changeSenseConfiguration.getMaximumSizeOfFilesBytes()),
                    byteCountToDisplaySize(fileToRead.length())));
        }
    }

    /**
     * Cast {@link ChangedFile} to {@link FilesystemChangedFile} safely.
     *
     * @param changedFile Changed file.
     * @return FilesystemChangedFile instance.
     */
    private FilesystemChangedFile checkAndCast(final ChangedFile changedFile) {
        if (!(changedFile instanceof FilesystemChangedFile)) {
            throw new IllegalStateException(String.format("Unexpected ChangedFile implementation: '%s'",
                    changedFile.getClass().getCanonicalName()));
        }
        return (FilesystemChangedFile) changedFile;
    }

    @Override
    public byte[] fetchNewRevision(final ChangedFile changedFile) {
        final FilesystemChangedFile file = checkAndCast(changedFile);
        final File fileToRead = file.getNewRevisionFile();
        return readFile(fileToRead);
    }

    /**
     * Determine changes in files between two directory trees.
     *
     * @param revA            File instance pointing to a directory containing original revisions of files.
     * @param revB            File instance pointing to a directory containing new revisions of files.
     * @param filenamePattern File paths have to match this file name pattern to be examined.
     * @return a list of files that matched the file name pattern.
     */
    private List<ChangedFile> determineChangedFiles(final File revA, final File revB, final Pattern filenamePattern) {
        final Map<String, File> revAFiles = getRevisionFileMap(revA, filenamePattern);
        final Map<String, File> revBFiles = getRevisionFileMap(revB, filenamePattern);
        return determine(revA, revB, revAFiles, revBFiles);
    }

    /**
     * Get map of files recursively found in a given revision directory.
     *
     * @param revision        File pointing to the revision's directory.
     * @param filenamePattern List only files that match against this pattern.
     * @return mapping of revision-relative file paths to file objects.
     */
    private Map<String, File> getRevisionFileMap(final File revision, final Pattern filenamePattern) {
        final List<File> revAList = listFiles(revision, filenamePattern);
        final Map<String, File> revAFiles;
        if (revAList != null) {
            revAFiles = revAList.stream()
                    .collect(Collectors.toMap(f -> prepareFilenameMapKey(revision, f),
                            Function.identity()));
        } else {
            revAFiles = new HashMap<>();
        }
        return revAFiles;
    }

    /**
     * List all files of a revision recursively.
     *
     * @param revision        File pointing to the revision's directory.
     * @param filenamePattern List only files that match against this pattern.
     * @return a list of files.
     */
    private List<File> listFiles(final File revision, final Pattern filenamePattern) {
        return listFilesRecursion(0, revision, filenamePattern);
    }

    /**
     * Recursion to list files from revision directory until done or maximum recursion depth reached.
     *
     * @param depth           Current depth (starts with 0).
     * @param directory       Current directory to list.
     * @param filenamePattern List only files that match against this pattern.
     * @return a list of files.
     */
    private List<File> listFilesRecursion(final int depth, final File directory,
                                          final Pattern filenamePattern) {
        if (depth > MAX_NESTED_DIRECTORIES) {
            throw new IllegalStateException(String.format("Maxmimum number of nested directories (%d) exceeded!",
                    MAX_NESTED_DIRECTORIES));
        }
        if (!directory.isDirectory()) {
            throw new IllegalArgumentException(String.format("'%s' is not a directory!", directory));
        }
        final File[] currentDirList = directory.listFiles(
                (file, filename) -> filenamePattern.matcher(filename).matches() || file.isDirectory());
        if (currentDirList != null) {
            return Arrays.stream(currentDirList)
                    .flatMap(file -> {
                        if (file.isDirectory()) {
                            return listFilesRecursion(depth + 1, file, filenamePattern).stream();
                        }
                        return Stream.of(file);
                    })
                    .collect(Collectors.toList());
        }
        return Collections.emptyList();
    }

    /**
     * Determine changes between two revisions using file maps.
     *
     * @param rootPathA Root path of original revision.
     * @param rootPathB Root path of new revision.
     * @param revAFiles Map of files in original revision.
     * @param revBFiles Map of files in new revision.
     * @return a list of changed files.
     */
    private List<ChangedFile> determine(final File rootPathA, final File rootPathB, final Map<String, File> revAFiles,
                                        final Map<String, File> revBFiles) {
        final List<ChangedFile> changes = revBFiles.entrySet().stream()
                .map(entry -> determineAddFileChange(rootPathA, rootPathB, entry.getKey()))
                .collect(Collectors.toList());
        changes.addAll(revAFiles.entrySet().stream()
                .filter(entry -> !revBFiles.containsKey(entry.getKey()))
                .map(entry -> determineRemoveFileChange(rootPathA, rootPathB, entry.getKey()))
                .collect(Collectors.toList()));
        return changes;
    }

    /**
     * Create changed file that has been added between original and new revision.
     *
     * @param rootPathA        Root path of original revision.
     * @param rootPathB        Root path of new revision.
     * @param relativeFilePath Relative file path of file in question.
     * @return a changed file.
     */
    private ChangedFile determineAddFileChange(final File rootPathA, final File rootPathB, final String relativeFilePath) {
        return new FilesystemChangedFile(this, rootPathA, rootPathB, relativeFilePath);
    }

    /**
     * Create changed file that has been removed between original and new revision.
     *
     * @param rootPathA        Root path of original revision.
     * @param rootPathB        Root path of new revision.
     * @param relativeFilePath Relative file path of file in question.
     * @return a changed file.
     */
    private ChangedFile determineRemoveFileChange(final File rootPathA, final File rootPathB, final String relativeFilePath) {
        return new FilesystemChangedFile(this, rootPathA, rootPathB, relativeFilePath);
    }

    /**
     * Prepare key (relative path) for file of a revision.
     *
     * @param revision Root path of revision.
     * @param f        File path.
     * @return relative path of file in relation to root path.
     */
    private String prepareFilenameMapKey(final File revision, final File f) {
        final String normalizedFilePath = FilenameUtils.normalizeNoEndSeparator(f.getAbsolutePath(), true);
        final String normalizedRevisionPath = FilenameUtils.normalizeNoEndSeparator(revision.getAbsolutePath(), true) + "/";
        if (normalizedFilePath.indexOf(normalizedRevisionPath) != 0) {
            throw new IllegalArgumentException(String.format("File '%s' is not in repo base path '%s'!",
                    normalizedFilePath, normalizedRevisionPath));
        }
        return normalizedFilePath.substring(normalizedRevisionPath.length());
    }

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
