package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeRepository;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Objects;

import static java.util.Objects.nonNull;

/**
 * Changed file implementation directly reading from file system.
 */
public class FilesystemChangedFile extends AbstractChangedFile {
    /**
     * File path relative to the revision root.
     */
    private final String filename;

    /**
     * {@link net.trurl.changesense.model.ChangedFile.ChangeType} as int.
     */
    private final int changeType;

    /**
     * Path of directory containing original revisions.
     */
    private final File rootPathA;
    /**
     * Path of directory containing new revisions.
     */
    private final File rootPathB;

    /**
     * Create new instance and detect changes.
     *
     * @param changeRepository Parent repository.
     * @param rootPathA        Path of directory containing original revisions of files.
     * @param rootPathB        Path of directory containing new revisions of files.
     * @param filename         Relative path to file.
     */
    FilesystemChangedFile(final ChangeRepository changeRepository, final File rootPathA, final File rootPathB,
                          final String filename) {
        super(changeRepository);
        Objects.requireNonNull(rootPathA);
        Objects.requireNonNull(rootPathB);
        this.filename = filename;
        this.rootPathA = rootPathA;
        this.rootPathB = rootPathB;
        changeType = determineChangeType(getOriginalRevisionFile(), getNewRevisionFile());
    }

    /**
     * Determine change type between original and new revision.
     *
     * @param revA Original revision.
     * @param revB New revision.
     * @return ChangeType as integer.
     */
    private int determineChangeType(final File revA, final File revB) {
        if (nonNull(revA) && revA.exists() && nonNull(revB) && revB.exists()) {
            return compareContents(revA, revB);
        }
        if (nonNull(revA) && revA.exists()) {
            return ChangeType.REMOVED.getType();
        }
        if (nonNull(revB) && revB.exists()) {
            return ChangeType.ADDED.getType();
        }
        return 0;
    }

    /**
     * Compare file contents (checksum) to determine if file has changed.
     *
     * @param revA Original revision (must exist).
     * @param revB New revision (must exist).
     * @return ChangeType as integer.
     */
    private int compareContents(final File revA, final File revB) {
        try {
            if (revA.length() != revB.length() || FileUtils.checksumCRC32(revA) != FileUtils.checksumCRC32(revB)) {
                return ChangeType.CHANGED.getType();
            }
        } catch (final IOException e) {
            throw new UncheckedIOException(e);
        }
        return 0;
    }

    @Override
    public String getFilename() {
        return filename;
    }

    @Override
    public int getChangeType() {
        return changeType;
    }

    /**
     * Get original revision.
     *
     * @return file denoting original revision.
     */
    File getOriginalRevisionFile() {
        return new File(rootPathA, filename);
    }

    /**
     * Get new revision.
     *
     * @return file denoting new revision.
     */
    File getNewRevisionFile() {
        return new File(rootPathB, filename);
    }
}
