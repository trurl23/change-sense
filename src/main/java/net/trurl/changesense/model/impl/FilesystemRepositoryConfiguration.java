package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.RepositoryConfiguration;

/**
 * Repository configuration object for {@link FilesystemChangeRepository}.
 * <p>
 *       Use an instance of this configuration class to obtain an instance of a {@link GitChangeRepository} via a call to
 *   {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} on the
 *   {@link DefaultRepositoryService} which you in turn can get via
 *   {@link net.trurl.changesense.service.RepositoryService#createDefaultRepositoryService()}.
 */
public class FilesystemRepositoryConfiguration implements RepositoryConfiguration {
}
