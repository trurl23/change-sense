package net.trurl.changesense.model.impl;

import net.trurl.changesense.exception.ChangeSenseGitException;
import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.ChangedFile;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.service.RepositoryService;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.internal.storage.file.FileRepository;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Implementation of a {@link ChangeRepository} for git.
 * <p>
 * A git based change repository compares the state of any two valid git revision references by utilizing a (bare)
 * repository passed in as <code>repoDir</code> when creating the repository instance.
 * <p>
 * Use {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} on the
 * {@link DefaultRepositoryService} with a {@link GitRepositoryConfiguration} object to obtain an instance of this
 * repository type. You can get the default repository service via a call to
 * {@link RepositoryService#createDefaultRepositoryService()}.
 */
public class GitChangeRepository implements ChangeRepository {
    private static final Logger LOGGER = LoggerFactory.getLogger(GitChangeRepository.class);

    /**
     * Directory where the repo is created.
     */
    private final File repoDir;

    /**
     * JGit repository.
     */
    private final Repository repository;

    /**
     * Configuration object.
     */
    private final ChangeSenseConfiguration changeSenseConfiguration;

    /**
     * Create a new git change repository with an existing git repository in the supplied repo dir.
     *
     * @param changeSenseConfiguration Configuration object.
     * @param repoDir                  Directory containing the git repository.
     */
    public GitChangeRepository(final ChangeSenseConfiguration changeSenseConfiguration,
                               final File repoDir) {
        this.changeSenseConfiguration = changeSenseConfiguration;
        this.repoDir = repoDir;
        try {
            repository = new FileRepository(repoDir);
        } catch (final IOException e) {
            throw new ChangeSenseGitException(String.format("Error creating file repository '%s'!", repoDir), e);
        }
    }

    @Override
    public List<ChangedFile> calcChangedFiles(final String revisionA, final String revisionB,
                                              final Pattern filenamePattern) {
        try (final Git git = new Git(repository)) {
            final ObjectId revA = repository.resolve(revisionA);
            final ObjectId revB = repository.resolve(revisionB);
            checkReferenceResolved(revisionA, revA);
            checkReferenceResolved(revisionB, revB);
            final LinkedList<RevCommit> commits = new LinkedList<>();
            git.log()
                    .addRange(revA, revB)
                    .call().forEach(commits::add);
            final List<DiffEntry> diffEntries = git.diff()
                    .setOldTree(createTreeParser(commits.getLast().getTree()))
                    .setNewTree(createTreeParser(commits.getFirst().getTree()))
                    .call();
            return diffEntries.stream()
                    .filter(diffEntry -> filenameMatches(filenamePattern, diffEntry))
                    .map(diffEntry -> new GitChangedFile(diffEntry, this))
                    .collect(Collectors.toList());
        } catch (final GitAPIException | IOException e) {
            throw new ChangeSenseGitException(String.format("Error listing tags of repository '%s'!", repoDir), e);
        }
    }

    @Override
    public boolean tryResolveReference(final String revision) {
        if (StringUtils.isBlank(revision)) {
            throw new IllegalArgumentException("Revision reference must not be blank!");
        }
        try {
            checkReferenceResolved(revision, repository.resolve(revision));
        } catch (final IOException | ChangeSenseGitException ex) {
            LOGGER.debug("Error resolving git repository reference '{}'!", revision, ex);
            return false;
        }
        return true;
    }

    /**
     * Check whether the filename pattern matches the or and new paths from diff entry.
     *
     * @param filenamePattern Filename pattern to apply (may be null).
     * @param diffEntry       Git diff entry.
     * @return true if the pattern matches (or was null), false otherwise.
     */
    private boolean filenameMatches(final Pattern filenamePattern, final DiffEntry diffEntry) {
        return filenamePattern == null || diffEntry.getOldPath() != null
                && filenamePattern.matcher(diffEntry.getOldPath()).matches()
                || diffEntry.getNewPath() != null && filenamePattern.matcher(
                diffEntry.getNewPath()).matches();
    }

    /**
     * Ensure that the given revision has been resolved to the specified object ID. Throw an exception otherwise.
     *
     * @param revision   Name of revision.
     * @param revisionId JGit object ID.
     */
    private void checkReferenceResolved(final String revision, final ObjectId revisionId) {
        if (revisionId == null) {
            throw new ChangeSenseGitException(String.format("Could not resolve reference '%s'!", revision));
        }
    }

    @Override
    public byte[] fetchOldRevision(final ChangedFile changedFile) {
        if (!(changedFile instanceof GitChangedFile)) {
            throw new IllegalArgumentException("Expected GitChangedFile!");
        }
        if (changedFile.is(ChangedFile.ChangeType.ADDED)) {
            return new byte[0];
        }
        final GitChangedFile gitChangedFile = (GitChangedFile) changedFile;
        final ObjectId objectId = gitChangedFile.getDiffEntry().getOldId().toObjectId();
        return fetchRevision(objectId);
    }

    @Override
    public byte[] fetchNewRevision(final ChangedFile changedFile) {
        if (!(changedFile instanceof GitChangedFile)) {
            throw new IllegalArgumentException("Expected GitChangedFile!");
        }
        if (changedFile.is(ChangedFile.ChangeType.REMOVED)) {
            return new byte[0];
        }
        final GitChangedFile gitChangedFile = (GitChangedFile) changedFile;
        final ObjectId objectId = gitChangedFile.getDiffEntry().getNewId().toObjectId();
        return fetchRevision(objectId);
    }

    /**
     * Fetch specified object from git.
     *
     * @param objectId Object ID of object to fetch.
     * @return Bytes of the object, up to the configured value.
     */
    private byte[] fetchRevision(final ObjectId objectId) {
        try {
            final ObjectLoader objectLoader = repository.open(objectId);
            return objectLoader.getBytes(changeSenseConfiguration.getMaximumSizeOfFilesBytes());
        } catch (final IOException e) {
            throw new ChangeSenseGitException(String.format("Error getting revision '%s'!",
                    objectId.toString()), e);
        }
    }

    /**
     * Helper method to create a canonical tree parser for the given object ID.
     *
     * @param revision Start revision of the tree parser.
     * @return a canonical tree parser.
     * @throws IOException if the underlying JGit library fails.
     */
    private CanonicalTreeParser createTreeParser(final ObjectId revision) throws IOException {
        final CanonicalTreeParser tree = new CanonicalTreeParser();
        try (final ObjectReader reader = repository.newObjectReader()) {
            tree.reset(reader, revision);
        }
        return tree;
    }

    /**
     * Returns git to work on the repository directly.
     *
     * @return Git instance.
     */
    public Git getGit() {
        return new Git(repository);
    }

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
