package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangedFile;
import org.eclipse.jgit.diff.DiffEntry;

import java.util.LinkedList;
import java.util.List;

/**
 * An implementation of {@link ChangedFile} for a git repository.
 */
public class GitChangedFile extends AbstractChangedFile {
    /**
     * Diff entry describing the change from a GIT perspective.
     */
    private final DiffEntry diffEntry;

    /**
     * Create a new {@link ChangedFile} from a JGit diff-entry.
     *
     * @param diffEntry        JGit diff-entry.
     * @param changeRepository Change repository the file belongs to.
     */
    GitChangedFile(final DiffEntry diffEntry, final ChangeRepository changeRepository) {
        super(changeRepository);
        this.diffEntry = diffEntry;
    }

    @Override
    public String getFilename() {
        return diffEntry.getNewPath();
    }

    @Override
    public int getChangeType() {
        final List<ChangeType> changeTypes = new LinkedList<>();
        if (diffEntry.getNewMode() != null && diffEntry.getNewMode().equals(diffEntry.getOldMode())) {
            changeTypes.add(ChangeType.MODE);
        }
        switch (diffEntry.getChangeType()) {
            case ADD:
                changeTypes.add(ChangeType.CHANGED);
                changeTypes.add(ChangeType.ADDED);
                break;
            case COPY:
                changeTypes.add(ChangeType.COPIED);
                break;
            case DELETE:
                changeTypes.add(ChangeType.CHANGED);
                changeTypes.add(ChangeType.REMOVED);
                break;
            case MODIFY:
                changeTypes.add(ChangeType.CHANGED);
                break;
            case RENAME:
                changeTypes.add(ChangeType.MOVED);
                break;
        }
        return ChangeType.fromList(changeTypes);
    }

    /**
     * Get underlying git diff-entry.
     *
     * @return JGit diff-entry.
     */
    DiffEntry getDiffEntry() {
        return diffEntry;
    }

    @Override
    public String toString() {
        return getChangeType() + " " + getFilename();
    }
}
