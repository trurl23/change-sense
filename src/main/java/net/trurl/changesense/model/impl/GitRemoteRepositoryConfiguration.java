package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.RepositoryConfiguration;
import org.eclipse.jgit.transport.CredentialsProvider;

import java.io.File;

/**
 * Repository configuration for a {@link GitChangeRepository} that is cloned or brought up-to-date from a remote git
 * repository.
 * You have to provide a suitable {@link CredentialsProvider} implementation. See
 * {@link net.trurl.changesense.ssh.SshCredentialsProvider} if you are using private key based authentication via SSH or
 * the other supplied credentials provider implementations that come with JGit, as
 * {@link org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider} for example.
 * <p>
 * Because the underlying repository is a {@link GitChangeRepository} you'll also have to supply a  <code>repoDir</code>
 * where the repository's bare git repository will be stored. In addition a remote URL to clone or update the repository
 * has to be specified. If the specified <code>repoDir</code> already exists it will be brought up-to-date from the
 * remote (which has to exist matching the supplied <code>remoteUrl</code>).
 * <p>
 * Use an instance of this configuration class to obtain an instance of a {@link GitChangeRepository} via a call to
 * {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} on the
 * {@link DefaultRepositoryService} which you in turn can get via
 * {@link net.trurl.changesense.service.RepositoryService#createDefaultRepositoryService()}.
 */
public class GitRemoteRepositoryConfiguration extends GitRepositoryConfiguration {

    /**
     * Credentials provider to use.
     */
    private final CredentialsProvider credentialsProvider;

    /**
     * Remote URL of repository.
     */
    private final String remoteUrl;

    /**
     * Create new remote git change repository configuration.
     *
     * @param repoDir             File path where to clone the remote repo. If it already exists, the repo will be
     *                            brought up-to-date, but this requires that a remote matching <code>remoteUrl</code> is
     *                            already defined.
     * @param credentialsProvider Credentials provider to use.
     * @param remoteUrl           URL of git remote.
     */
    public GitRemoteRepositoryConfiguration(final File repoDir, final CredentialsProvider credentialsProvider,
                                            final String remoteUrl) {
        super(repoDir);
        this.credentialsProvider = credentialsProvider;
        this.remoteUrl = remoteUrl;
    }

    /**
     * Get credentials provider to use.
     *
     * @return credentials provider.
     */
    public CredentialsProvider getCredentialsProvider() {
        return credentialsProvider;
    }

    /**
     * Get git remote server clone URL.
     *
     * @return remote URL.
     */
    public String getRemoteUrl() {
        return remoteUrl;
    }
}
