package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.RepositoryConfiguration;

import java.io.File;

/**
 * Configuration object for a local {@link GitChangeRepository}. If you want to work with remote repositories, take a
 * look at {@link GitRemoteRepositoryConfiguration}.
 * <p>
 * Use an instance of this configuration class to obtain an instance of a {@link GitChangeRepository} via a call to
 * {@link net.trurl.changesense.service.RepositoryService#createChangeRepository(RepositoryConfiguration)} on the
 * {@link DefaultRepositoryService} which you in turn can get via
 * {@link net.trurl.changesense.service.RepositoryService#createDefaultRepositoryService()}.
 */
public class GitRepositoryConfiguration implements RepositoryConfiguration {
    /**
     * Local repository directory.
     */
    private final File repoDir;

    /**
     * Create new configuration.
     *
     * @param repoDir Local directory where git repository is located. Repository may be a bare repository.
     */
    public GitRepositoryConfiguration(final File repoDir) {
        this.repoDir = repoDir;
    }

    /**
     * Get local git repository directory.
     *
     * @return directory.
     */
    public File getRepoDir() {
        return repoDir;
    }
}
