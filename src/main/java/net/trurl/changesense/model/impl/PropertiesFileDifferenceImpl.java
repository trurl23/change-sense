package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangedFile;
import net.trurl.changesense.model.PropertiesFileDifference;
import net.trurl.changesense.model.PropertyDifference;

import java.util.List;
import java.util.stream.Collectors;

/**
 * A {@link PropertiesFileDifferenceImpl} stores a list of properties of a property file that were touched by a VCS
 * commit.
 */
public class PropertiesFileDifferenceImpl implements PropertiesFileDifference {
    /**
     * Underlying changed file.
     */
    private final ChangedFile changedFile;

    /**
     * List of differences contained in the change.
     */
    private final List<PropertyDifference> properties;

    /**
     * Create a new properties file difference instance from a {@link ChangedFile} and a list of
     * {@link PropertyDifference} implementations.
     *
     * @param changedFile A {@link ChangedFile} instance.
     * @param properties  A list of {@link PropertyDifference} objects.
     */
    public PropertiesFileDifferenceImpl(final ChangedFile changedFile, final List<PropertyDifference> properties) {
        this.changedFile = changedFile;
        this.properties = properties;
    }

    @Override
    public List<PropertyDifference> getProperties() {
        return properties;
    }

    @Override
    public List<PropertyDifference> getProperties(final PropertyDifference.DifferenceType... differenceTypes) {
        return properties.stream()
                .filter(prop -> prop.getDifferenceType().isAnyOf(differenceTypes))
                .collect(Collectors.toList());
    }

    @Override
    public ChangedFile getFile() {
        return changedFile;
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder()
                .append(changedFile)
                .append(System.lineSeparator());
        properties.forEach(prop -> builder.append(prop)
                .append(System.lineSeparator()));
        return builder.toString();
    }
}
