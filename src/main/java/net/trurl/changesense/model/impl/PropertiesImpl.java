package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.Configurable;
import net.trurl.changesense.model.Properties;
import net.trurl.changesense.model.Property;
import net.trurl.changesense.service.parser.PropertyParser;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * One property from a property file along with its documentation in the form of comments.
 */
public class PropertiesImpl implements Properties, Configurable {
    private Map<String, Property> properties = new HashMap<>();

    private final ChangeSenseConfiguration changeSenseConfiguration;

    public PropertiesImpl(final ChangeSenseConfiguration changeSenseConfiguration) {
        this.changeSenseConfiguration = changeSenseConfiguration;
    }

    @Override
    public List<Property> getProperties() {
        return new LinkedList<>(properties.values());
    }

    @Override
    public boolean hasProperty(final String name) {
        return properties.containsKey(name);
    }

    @Override
    public boolean readFrom(final InputStream inputStream, final String parserId) throws IOException {
        final Class<? extends PropertyParser> propertyParserClass = changeSenseConfiguration
                .getParser(parserId);
        if (propertyParserClass == null) {
            return false;
        }
        final PropertyParser propertyParser;
        try {
            propertyParser = propertyParserClass.newInstance();
        } catch (final InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException(String.format("Error instantiating property parser from class %s",
                    propertyParserClass.getCanonicalName()));
        }
        properties = propertyParser.readFrom(inputStream);
        return true;
    }

    @Override
    public Property getProperty(final String name) {
        return properties.get(name);
    }

    @Override
    public String toString() {
        return new ReflectionToStringBuilder(this).build();
    }

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
