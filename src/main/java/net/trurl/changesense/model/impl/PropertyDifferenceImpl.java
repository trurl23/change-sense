package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.Property;
import net.trurl.changesense.model.PropertyDifference;

/**
 * Stores difference of a property along with documentation between two revisions.
 */
public class PropertyDifferenceImpl implements PropertyDifference {
    /**
     * Type of property difference.
     */
    private final DifferenceType differenceType;
    /**
     * State of property at revision A.
     */
    private final Property before;
    /**
     * State of property at revision B.
     */
    private final Property after;

    /**
     * Create new property difference instance.
     *
     * @param differenceType Type of difference.
     * @param before         State at revision A.
     * @param after          State at revision B.
     */
    public PropertyDifferenceImpl(final DifferenceType differenceType, final Property before, final Property after) {
        this.differenceType = differenceType;
        this.before = before;
        this.after = after;
    }

    @Override
    public DifferenceType getDifferenceType() {
        return differenceType;
    }

    @Override
    public Property getBefore() {
        return before;
    }

    @Override
    public Property getAfter() {
        return after;
    }

    @Override
    public String toString() {
        return String.valueOf(differenceType) +
                System.lineSeparator() +
                (before != null ?
                        "Before:" +
                                System.lineSeparator() +
                                before +
                                System.lineSeparator() : "") +
                (after != null ?
                        "After:" +
                                System.lineSeparator() +
                                after +
                                System.lineSeparator() : "");

    }
}
