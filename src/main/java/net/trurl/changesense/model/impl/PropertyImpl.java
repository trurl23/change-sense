package net.trurl.changesense.model.impl;

import net.trurl.changesense.model.Property;

import java.util.Objects;

/**
 * A property including its documentation in the form of comments.
 */
public class PropertyImpl implements Property {
    /**
     * Property name.
     */
    private final String name;

    /**
     * Property value.
     */
    private final String value;

    /**
     * Property documentation.
     */
    private final String documentation;

    /**
     * Create a new property instance.
     *
     * @param name          Name of property.
     * @param value         Value of property.
     * @param documentation Property documentation comments.
     */
    public PropertyImpl(final String name, final String value, final String documentation) {
        this.name = name;
        this.value = value;
        this.documentation = documentation;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public String getDocumentation() {
        return documentation;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final PropertyImpl property = (PropertyImpl) o;
        return Objects.equals(name, property.name) &&
                Objects.equals(value, property.value) &&
                Objects.equals(documentation, property.documentation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, value, documentation);
    }

    @Override
    public String toString() {
        return documentation + name + '=' + value;
    }
}
