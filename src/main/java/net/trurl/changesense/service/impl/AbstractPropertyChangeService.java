package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.*;
import net.trurl.changesense.model.impl.PropertiesFileDifferenceImpl;
import net.trurl.changesense.model.impl.PropertyDifferenceImpl;
import net.trurl.changesense.service.PropertyChangeService;
import org.apache.commons.io.FilenameUtils;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Abstract base class for property change services. You can registerDelegate a different parser than the default one via
 * implementing {@link ChangeSenseConfiguration#getParser(String)} or via
 * {@link net.trurl.changesense.model.impl.DefaultChangeSenseConfiguration#registerParser(String, Class)} if you're
 * using the defaults or extend the defaults.
 * parsing a file format of your choosing, as long as you can map everything to change-sense's {@link Property}
 * interface.
 */
public abstract class AbstractPropertyChangeService extends AbstractChangeService implements PropertyChangeService,
        Configurable {
    private final ChangeSenseConfiguration changeSenseConfiguration;

    public AbstractPropertyChangeService(final ChangeSenseConfiguration changeSenseConfiguration) {
        this.changeSenseConfiguration = changeSenseConfiguration;
    }

    @Override
    public List<PropertiesFileDifference> getPropertyDifferences(final ChangeRepository repository,
                                                                 final String revisionA, final String revisionB,
                                                                 final String... propertyPaths) {
        return calcChangedFiles(repository, revisionA, revisionB, ".properties", propertyPaths).stream()
                .map(this::evaluateFileChange)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .sorted(Comparator.comparing(o -> o.getFile().getFilename()))
                .collect(Collectors.toList());
    }

    /**
     * Creates a properties file difference instance depending on whether the {@link ChangedFile} has changes in
     * contents.
     *
     * @param changedFile Changed file instance.
     * @return an object describing the property differences within the file, if the content of the file was modified.
     * An empty optional otherwise.
     */
    private Optional<PropertiesFileDifference> evaluateFileChange(final ChangedFile changedFile) {
        if (changedFile.is(ChangedFile.ChangeType.CHANGED)) {
            return Optional.of(evaluatePropertyDifferenceChange(changedFile));
        } else if (changedFile.is(ChangedFile.ChangeType.ADDED)) {
            return Optional.of(evaluatePropertyAddChange(changedFile));
        } else if (changedFile.is(ChangedFile.ChangeType.REMOVED)) {
            return Optional.of(evaluatePropertyRemoveChange(changedFile));
        }
        return Optional.empty();
    }

    /**
     * Evaluate property differences in a changed file (must ensure it has the change type
     * {@link ChangedFile.ChangeType#CHANGED}).
     *
     * @param changedFile Changed file instance.
     * @return an object describing the property differences within the file.
     */
    protected PropertiesFileDifference evaluatePropertyDifferenceChange(final ChangedFile changedFile) {
        final byte[] oldContents = changedFile.fetchOldRevision();
        final byte[] newContents = changedFile.fetchNewRevision();
        // use extension as parser ID
        final String ext = FilenameUtils.getExtension(changedFile.getFilename());
        final Properties oldProps = readProperties(oldContents, ext);
        final Properties newProps = readProperties(newContents, ext);
        final List<PropertyDifference> propertyDifferences = new LinkedList<>();
        propertyDifferences.addAll(findAddedProperties(oldProps, newProps));
        propertyDifferences.addAll(findRemovedProperties(oldProps, newProps));
        propertyDifferences.addAll(findChangedProperties(oldProps, newProps));
        return new PropertiesFileDifferenceImpl(changedFile, propertyDifferences);
    }

    protected PropertiesFileDifference evaluatePropertyAddChange(final ChangedFile changedFile) {
        final String ext = FilenameUtils.getExtension(changedFile.getFilename());
        final Properties props = readProperties(changedFile.fetchNewRevision(), ext);
        final List<PropertyDifference> differences = props.getProperties().stream()
                .map(property -> new PropertyDifferenceImpl(PropertyDifference.DifferenceType.ADDED, null, property))
                .collect(Collectors.toList());
        return new PropertiesFileDifferenceImpl(changedFile, differences);
    }

    protected PropertiesFileDifference evaluatePropertyRemoveChange(final ChangedFile changedFile) {
        final String ext = FilenameUtils.getExtension(changedFile.getFilename());
        final Properties props = readProperties(changedFile.fetchOldRevision(), ext);
        final List<PropertyDifference> differences = props.getProperties().stream()
                .map(property -> new PropertyDifferenceImpl(PropertyDifference.DifferenceType.REMOVED, property, null))
                .collect(Collectors.toList());
        return new PropertiesFileDifferenceImpl(changedFile, differences);
    }

    /**
     * Find added properties and return them as {@link PropertyDifference} objects.
     *
     * @param oldProps Old revision.
     * @param newProps New revision.
     * @return a list containing objects describing the added properties.
     */
    private List<PropertyDifference> findAddedProperties(final Properties oldProps, final Properties newProps) {
        return newProps.getProperties().stream()
                .filter(property -> !oldProps.hasProperty(property.getName()))
                .map(property -> new PropertyDifferenceImpl(PropertyDifference.DifferenceType.ADDED, null, property))
                .collect(Collectors.toList());
    }

    /**
     * Find properties that were removed and return them as {@link PropertyDifference} objects.
     *
     * @param oldProps Old revision.
     * @param newProps New revision.
     * @return a list containing objects describing the removed properties.
     */
    private List<PropertyDifference> findRemovedProperties(final Properties oldProps, final Properties newProps) {
        return oldProps.getProperties().stream()
                .filter(property -> !newProps.hasProperty(property.getName()))
                .map(property -> new PropertyDifferenceImpl(PropertyDifference.DifferenceType.REMOVED, property, null))
                .collect(Collectors.toList());
    }

    /**
     * Find modified properties and return them as {@link PropertyDifference} objects.
     *
     * @param oldProps Old revision.
     * @param newProps New revision.
     * @return a list containing objects describing the modified properties.
     */
    private List<PropertyDifference> findChangedProperties(final Properties oldProps, final Properties newProps) {
        return oldProps.getProperties().stream()
                .filter(property -> newProps.hasProperty(property.getName()))
                .map(oldProp -> {
                    final PropertyDifference.DifferenceType differenceType;
                    if (!newProps.getProperty(oldProp.getName()).equals(oldProp)) {
                        differenceType = PropertyDifference.DifferenceType.CHANGED;
                    } else {
                        differenceType = PropertyDifference.DifferenceType.UNCHANGED;
                    }
                    return new PropertyDifferenceImpl(differenceType, oldProp,
                            newProps.getProperty(oldProp.getName()));
                }).collect(Collectors.toList());
    }

    /**
     * Read properties from byte array.
     *
     * @param contents Property file contents.
     * @param parserId Parser ID to use (=file extension without the dot per convention).
     * @return the properties object.
     */
    protected abstract Properties readProperties(byte[] contents, String parserId);

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
