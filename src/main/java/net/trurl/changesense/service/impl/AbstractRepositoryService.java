package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.service.RepositoryService;

/**
 * Abstract base class of a repository service. A repository service can be used to obtain a concrete implementation of
 * a {@link ChangeRepository} using a matching {@link RepositoryConfiguration} object.
 * <p>
 * Each implementation is made known to {@link net.trurl.changesense.model.impl.DefaultRepositoryService} which
 * delegates creation of {@link ChangeRepository} instances to the correct implementation depending on the instance
 * of {@link RepositoryConfiguration} encountered in the call to
 * {@link RepositoryService#createChangeRepository(RepositoryConfiguration)}.
 *
 * @param <T> Configuration class type accepted by this repository service.
 */
public abstract class AbstractRepositoryService<T extends RepositoryConfiguration> implements RepositoryService {
    private final ChangeSenseConfiguration changeSenseConfiguration;

    private final Class<T> configurationClass;

    /**
     * Create a new repository service.
     *
     * @param changeSenseConfiguration System configuration.
     * @param configurationClass       Configuration class accepted by this service.
     */
    public AbstractRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration, final Class<T> configurationClass) {
        this.changeSenseConfiguration = changeSenseConfiguration;
        this.configurationClass = configurationClass;
    }

    @Override
    public boolean accepts(final Class<? extends RepositoryConfiguration> configurationClazz) {
        if (configurationClazz == null) {
            throw new IllegalArgumentException("Configuration class must not be null!");
        }
        return configurationClazz.isAssignableFrom(getConfigurationClass());
    }

    @Override
    public ChangeRepository createChangeRepository(final RepositoryConfiguration repositoryConfiguration) {
        if (!(configurationClass.isInstance(repositoryConfiguration))) {
            throw new IllegalArgumentException("Unsupported configuration class!");
        }
        return doCreateChangeRepository((T) repositoryConfiguration);
    }

    /**
     * Override this method to create a change repository from a configuration object instance.
     *
     * @param repositoryConfiguration Configuration object instance.
     * @return a change repository.
     */
    protected abstract ChangeRepository doCreateChangeRepository(final T repositoryConfiguration);

    /**
     * Get accepted configuration object class.
     *
     * @return class.
     */
    protected Class<? extends RepositoryConfiguration> getConfigurationClass() {
        return configurationClass;
    }

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
