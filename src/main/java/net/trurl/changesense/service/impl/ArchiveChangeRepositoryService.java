package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.impl.ArchiveChangeRepository;
import net.trurl.changesense.model.impl.ArchiveRepositoryConfiguration;

/**
 * A repository service implementation that creates {@link ArchiveChangeRepository} instances from
 * {@link ArchiveRepositoryConfiguration} objects.
 */
public class ArchiveChangeRepositoryService extends AbstractRepositoryService<ArchiveRepositoryConfiguration> {
    public ArchiveChangeRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        super(changeSenseConfiguration, ArchiveRepositoryConfiguration.class);
    }

    @Override
    protected ChangeRepository doCreateChangeRepository(final ArchiveRepositoryConfiguration repositoryConfiguration) {
        return new ArchiveChangeRepository(getConfig());
    }

    @Override
    public String getName() {
        return "archive";
    }
}
