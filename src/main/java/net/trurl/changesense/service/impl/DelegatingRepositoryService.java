package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.RepositoryConfiguration;
import net.trurl.changesense.service.RepositoryService;

import java.util.LinkedList;
import java.util.List;

/**
 * A repository service that contains a collection of other {@link RepositoryService} instances and therefore is able
 * to accept all types of configuration objects that the registered repository services can handle by delegating
 * the work to them.
 * <p>
 * Delegate repository service instances can be registered by calling {@link #registerDelegate(RepositoryService)}.
 * <p>
 * Also see {@link net.trurl.changesense.model.impl.DefaultRepositoryService}.
 */
public class DelegatingRepositoryService implements RepositoryService {
    private final ChangeSenseConfiguration changeSenseConfiguration;

    private final List<RepositoryService> delegates = new LinkedList<>();

    public DelegatingRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        this.changeSenseConfiguration = changeSenseConfiguration;
    }

    /**
     * Register a new delegate repository service.
     *
     * @param repositoryService Delegate.
     */
    public void registerDelegate(final RepositoryService repositoryService) {
        if (repositoryService == null) {
            throw new IllegalArgumentException("Repository service must not be null!");
        }
        delegates.add(repositoryService);
    }

    /**
     * Get list of known delegates.
     *
     * @return list of delegates.
     */
    public List<RepositoryService> getDelegates() {
        return delegates;
    }

    @Override
    public String getName() {
        return "delegating";
    }

    @Override
    public boolean accepts(final Class<? extends RepositoryConfiguration> configurationClazz) {
        return delegates.stream()
                .anyMatch(delegate -> delegate.accepts(configurationClazz));
    }

    @Override
    public ChangeRepository createChangeRepository(final RepositoryConfiguration repositoryConfiguration) {
        if (repositoryConfiguration == null) {
            throw new IllegalArgumentException("Repository configuration must not be null!");
        }
        final Class<? extends RepositoryConfiguration> repositoryConfigurationClass = repositoryConfiguration.getClass();
        return delegates.stream()
                .filter(delegate -> delegate.accepts(repositoryConfigurationClass))
                .findFirst()
                .map(repositoryService -> repositoryService.createChangeRepository(repositoryConfiguration))
                .orElseThrow(() -> new IllegalArgumentException(String.format(
                        "No repository service found accepting configuration class '%s'!",
                        repositoryConfigurationClass.getCanonicalName())));
    }

    @Override
    public ChangeSenseConfiguration getConfig() {
        return changeSenseConfiguration;
    }
}
