package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.impl.FilesystemChangeRepository;
import net.trurl.changesense.model.impl.FilesystemRepositoryConfiguration;

/**
 * Repository service implementation that can create a {@link FilesystemChangeRepository} from a
 * {@link FilesystemRepositoryConfiguration}.
 */
public class FilesystemChangeRepositoryService extends AbstractRepositoryService<FilesystemRepositoryConfiguration> {
    public FilesystemChangeRepositoryService(final ChangeSenseConfiguration changeSenseConfiguration) {
        super(changeSenseConfiguration, FilesystemRepositoryConfiguration.class);
    }

    @Override
    protected ChangeRepository doCreateChangeRepository(final FilesystemRepositoryConfiguration repositoryConfiguration) {
        return new FilesystemChangeRepository(getConfig());
    }

    @Override
    public String getName() {
        return "filesystem";
    }
}
