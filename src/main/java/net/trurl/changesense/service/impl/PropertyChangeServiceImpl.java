package net.trurl.changesense.service.impl;

import net.trurl.changesense.exception.ChangeSensePropertiesException;
import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.ChangeSenseConfiguration;
import net.trurl.changesense.model.Properties;
import net.trurl.changesense.model.impl.PropertiesImpl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * A service operating on a {@link ChangeRepository} to provide details about changes in property files.
 */
public class PropertyChangeServiceImpl extends AbstractPropertyChangeService {

    public PropertyChangeServiceImpl(final ChangeSenseConfiguration changeSenseConfiguration) {
        super(changeSenseConfiguration);
    }

    @Override
    protected Properties readProperties(final byte[] contents, final String parserId) {
        final Properties properties = new PropertiesImpl(getConfig());
        try (final InputStream inputStream = new ByteArrayInputStream(contents)) {
            properties.readFrom(inputStream, parserId);
        } catch (final IOException e) {
            throw new ChangeSensePropertiesException("Error parsing properties file!", e);
        }
        return properties;
    }
}
