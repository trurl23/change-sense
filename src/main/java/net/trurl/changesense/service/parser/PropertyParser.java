package net.trurl.changesense.service.parser;

import net.trurl.changesense.model.Property;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

/**
 * Interface to be implemented by property parsers (to add support for different file formats).
 */
public interface PropertyParser {
    /**
     * Read properties from input stream and return them as a map.
     *
     * @param inputStream Input stream to read from.
     * @return mapping of keys to {@link Property} instances that were contained in the input stream data.
     * @throws IOException if the input stream could not be read.
     */
    Map<String, Property> readFrom(InputStream inputStream) throws IOException;
}
