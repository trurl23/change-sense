package net.trurl.changesense.service.parser.v1;

import net.trurl.changesense.model.Property;
import net.trurl.changesense.model.impl.PropertyImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.stream.Stream;

/**
 * Parser context. Has methods for reading and writing to three output buffers (documentation, name, value).
 */
public class Context {

    /**
     * Maximum character count that is buffered for unreading.
     */
    private static final int MAX_UNREAD = 10;

    public enum State {
        LOOKING,
        DOCUMENTATION,
        NAME,
        ESCAPE,
        VALUE,
        AWAITING_NEWLINE_IN_VALUE,
        AWAITING_NEWLINE_IN_DOC,
        SKIP_WHITESPACE,
        EOF
    }

    public enum Output {
        DOCUMENTATION,
        NAME,
        VALUE
    }


    private static final Logger LOGGER = LoggerFactory.getLogger(Context.class);

    private int column;
    private int line;
    private final InputStream inputStream;
    private final Deque<StateHolder> states = new LinkedList<>(Collections.singletonList(new StateHolder(State.LOOKING, Output.DOCUMENTATION)));
    private final StringBuilder documentation = new StringBuilder();
    private final StringBuilder name = new StringBuilder();
    private final StringBuilder value = new StringBuilder();
    private final Map<String, Property> properties = new LinkedHashMap<>();
    private final int[] backtrackBuffer = new int[MAX_UNREAD];
    private int backtrackPos = -1;

    Context(final InputStream inputStream) {
        this.inputStream = inputStream;
    }

    boolean isInState(final State... states) {
        return Stream.of(states)
                .anyMatch(state -> state == getCurrentState());
    }

    void pushState(final State state, final Output output) {
        states.addFirst(new StateHolder(state, output));
        if (LOGGER.isTraceEnabled()) {
            traceStackState("After push");
        }
    }

    void swapState(final State state, final Output output) {
        states.removeFirst();
        pushState(state, output);
    }

    void popState() {
        if (LOGGER.isDebugEnabled()) {
            traceStackState("Before pop");
        }
        if (states.size() < 2) {
            throw new IllegalStateException("State stack is empty!");
        }
        states.removeFirst();
    }

    private void traceStackState(final String message) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("{} {},{}", message, line, column);
            states.forEach(state -> LOGGER.trace(state.toString()));
        }
    }

    State getCurrentState() {
        return states.getFirst().getState();
    }

    Output getCurrentOutput() {
        return states.getFirst().getOutput();
    }

    public Map<String, Property> getProperties() {
        return properties;
    }

    private StringBuilder getOutputBuffer() {
        return getOutputBuffer(states.getFirst().getOutput());
    }

    private StringBuilder getOutputBuffer(final Output output) {
        switch (output) {
            case DOCUMENTATION:
                return documentation;
            case NAME:
                return name;
            case VALUE:
                return value;
            default:
                throw new IllegalStateException("Unknown/unset output!");
        }
    }

    public int getColumn() {
        return column;
    }

    private void addColumn() {
        column++;
    }

    public int getLine() {
        return line;
    }

    private void addLine() {
        column = 0;
        line++;
    }

    void eof() {
        pushState(State.EOF, getCurrentOutput());
    }

    void emitProperty() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Emitting property:");
            LOGGER.trace("\"{}\"", documentation.toString());
            LOGGER.trace("\"{} = {}\"", name.toString(), value.toString());
        }
        properties.put(name.toString(), new PropertyImpl(name.toString(),
                value.toString(), documentation.toString()));
        clear(Output.DOCUMENTATION);
        clear(Output.NAME);
        clear(Output.VALUE);
        popState();
    }

    void unreadValue(final int value) {
        if (backtrackPos >= MAX_UNREAD) {
            throw new IllegalStateException("Exceeded maximum rewind!");
        }
        backtrackBuffer[++backtrackPos] = value;
    }

    int readValue() throws IOException {
        if (backtrackPos >= 0) {
            return backtrackBuffer[backtrackPos--];
        }
        final int value = inputStream.read();
        if (value == -1) {
            pushState(State.EOF, getCurrentOutput());
            return value;
        } else {
            addColumn();
            if (value == '\n') {
                addLine();
            }
            return value;
        }
    }

    void append(final int value) {
        getOutputBuffer().append(Character.toChars(value));
    }

    void append(final String value) {
        getOutputBuffer().append(value);
    }

    boolean isOutputBufferEmpty() {
        return getOutputBuffer().length() == 0;
    }

    void clear(final Output output) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Clearing output buffer {}.", output);
        }
        final StringBuilder outputBuffer = getOutputBuffer(output);
        outputBuffer.delete(0, outputBuffer.length());
    }
}
