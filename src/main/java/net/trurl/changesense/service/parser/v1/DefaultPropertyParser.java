package net.trurl.changesense.service.parser.v1;

import net.trurl.changesense.model.Property;
import net.trurl.changesense.service.parser.PropertyParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import static java.lang.Character.isWhitespace;
import static java.lang.System.lineSeparator;
import static net.trurl.changesense.service.parser.v1.Context.State.*;

/**
 * FSM for custom parsing of property files.
 */
public class DefaultPropertyParser implements PropertyParser {

    @Override
    public Map<String, Property> readFrom(final InputStream inputStream) throws IOException {
        final Context context = new Context(inputStream);
        read(context);
        return context.getProperties();
    }

    /**
     * Main loop, depending on state the correct parsing module will be called.
     */
    private void read(final Context context) throws IOException {
        while (!context.isInState(Context.State.EOF)) {
            switch (context.getCurrentState()) {
                case LOOKING:
                    look(context);
                    break;
                case DOCUMENTATION:
                    readDoc(context);
                    break;
                case NAME:
                    readName(context);
                    break;
                case VALUE:
                    readValue(context);
                    break;
                case AWAITING_NEWLINE_IN_VALUE:
                    awaitNewlineInValue(context);
                    break;
                case AWAITING_NEWLINE_IN_DOC:
                    awaitNewlineInDoc(context);
                    break;
                case SKIP_WHITESPACE:
                    skipWhitespace(context);
                    break;
                case ESCAPE:
                    readEscape(context);
                    break;
                case EOF:
                    break;
                default:
                    throw new IllegalStateException(String.format("Unhandled state %s!", context.getCurrentState()));
            }
        }
    }

    private void skipWhitespace(final Context context) throws IOException {
        while (context.isInState(SKIP_WHITESPACE)) {
            final int value = context.readValue();
            switch (value) {
                case ' ':
                case '\f':
                case '\t':
                    break;
                default:
                    context.unreadValue(value);
                    context.popState();
            }

        }
    }

    /**
     * Expect a new line as the next character, skipping white-space and carriage return. Does not add anything to
     * the output buffer.
     */
    private void awaitNewlineInValue(final Context context) throws IOException {
        while (context.isInState(AWAITING_NEWLINE_IN_VALUE)) {
            final int value = context.readValue();
            if (value != -1) {
                if (value == '\n') {
                    context.emitProperty();
                    context.popState();
                } else if (!isWhitespace(value) && value != '\r') {
                    throw new IllegalStateException(String.format("Expected space or new line, was '%s'.",
                            String.valueOf(Character.toChars(value))));
                }
            } else {
                context.eof();
            }
        }
    }

    /**
     * Expect new line as the next character, but skip white-space and carriage return and handle escape sequences.
     * Add data that was read to the current output buffer.
     */
    private void awaitNewlineInDoc(final Context context) throws IOException {
        while (context.isInState(AWAITING_NEWLINE_IN_DOC)) {
            final int value = context.readValue();
            if (value != -1) {
                if (value == '\n') {
                    context.append(value);
                    context.popState();
                } else if (value == '\r') {
                    context.append(value);
                } else if (value == '\\') {
                    context.pushState(ESCAPE, context.getCurrentOutput());
                } else if (!isWhitespace(value)) {
                    throw new IllegalStateException(String.format("Expected space or new line, was '%s'.",
                            String.valueOf(Character.toChars(value))));
                }
            } else {
                context.eof();
            }
        }
    }

    /**
     * Skip whitespace, clear documentation on empty new line.
     */
    private void look(final Context context) throws IOException {
        while (context.isInState(Context.State.LOOKING)) {
            final int value = context.readValue();
            if (value != -1) {
                switch (value) {
                    case '#':
                    case '!':
                        context.unreadValue(value);
                        context.pushState(Context.State.DOCUMENTATION, Context.Output.DOCUMENTATION);
                        break;
                    case ' ':
                    case '\t':
                    case '\f':
                        context.pushState(Context.State.SKIP_WHITESPACE, context.getCurrentOutput());
                        break;
                    case '\r':
                        break;
                    case '\n':
                        context.clear(Context.Output.DOCUMENTATION);
                        break;
                    default:
                        context.pushState(Context.State.NAME, Context.Output.NAME);
                        context.unreadValue(value);
                }
            } else {
                context.eof();
            }
        }
    }

    /**
     * Read documentation and replace new lines wirth system dependent line separators.
     */
    private void readDoc(final Context context) throws IOException {
        while (context.isInState(Context.State.DOCUMENTATION)) {
            final int value = context.readValue();
            if (value != -1) {
                if (value == '\r') {
                    context.swapState(AWAITING_NEWLINE_IN_DOC, Context.Output.DOCUMENTATION);
                } else if (value == '\n') {
                    context.append(lineSeparator());
                    context.popState();
                } else {
                    context.append(value);
                }
            }
        }
    }

    /**
     * Read name of property, handle escape sequences. Switch mode to VALUE, when ':' or '=' is read.
     */
    private void readName(final Context context) throws IOException {
        while (context.isInState(Context.State.NAME)) {
            final int value = context.readValue();
            if (value != -1) {
                if (value == '\\') {
                    context.pushState(ESCAPE, context.getCurrentOutput());
                } else if (value == '=' || value == ':') {
                    context.pushState(Context.State.VALUE, Context.Output.VALUE);
                } else if (value == '\n') {
                    context.emitProperty();
                } else if (!isWhitespace(value)) {
                    context.append(value);
                }
            }
        }
    }

    /**
     * Read value of a property, handle escape sequences etc.
     */
    private void readValue(final Context context) throws IOException {
        while (context.isInState(Context.State.VALUE)) {
            final int value = context.readValue();
            if (value != -1) {
                if (value == '\\') {
                    context.pushState(Context.State.ESCAPE, context.getCurrentOutput());
                } else if (value == '\r') {
                    // waiting for a new-line
                    context.swapState(Context.State.AWAITING_NEWLINE_IN_VALUE, context.getCurrentOutput());
                } else if (value == '\n') {
                    // Construct property from data read up to this point.
                    context.emitProperty();
                    context.popState();
                } else if (!isWhitespace(value) || !context.isOutputBufferEmpty()) {
                    // Skip leading whitespace
                    context.append(value);
                }
            } else {
                context.eof();
            }
        }

    }

    /**
     * Read escape sequences (all started by a backslash). Assumes that the backslash is already read.
     */
    private void readEscape(final Context context) throws IOException {
        final Context.State origState = context.getCurrentState();
        while (context.isInState(origState)) {
            final int value = context.readValue();
            if (value != -1) {
                switch (value) {
                    case '\r':
                    case '\n':
                        context.popState();
                        break;
                    case 'r':
                        context.append('\r');
                        context.popState();
                        break;
                    case 'n':
                        context.append('\n');
                        context.popState();
                        break;
                    case 't':
                        context.append('\t');
                        context.popState();
                        break;
                    case '\\':
                        context.append('\\');
                        context.popState();
                        break;
                    case 'f':
                        context.append('\f');
                        context.popState();
                        break;
                    case 'b':
                        context.append('\b');
                        context.popState();
                        break;
                    case 'u':
                        parseUnicodeSequence(context);
                        context.popState();
                        break;
                    default:
                        context.append(value);
                        context.popState();
                }
            } else {
                context.eof();
            }
        }
    }

    /**
     * Construct unicode sequence (<code>\\uXXXX</code>) from the four hexadecimal input digits.
     */
    private void parseUnicodeSequence(final Context context) throws IOException {
        final StringBuilder code = new StringBuilder();
        for (int i = 0; i < 4; i++) {
            final int value = context.readValue();
            if (value != -1) {
                code.append(Character.toChars(value));
            } else {
                context.eof();
                return;
            }
        }
        context.append(Integer.parseInt(code.toString(), 16));
    }

}
