package net.trurl.changesense.service.parser.v1;

import net.trurl.changesense.service.parser.v1.Context.Output;
import net.trurl.changesense.service.parser.v1.Context.State;

public class StateHolder {
    private final State state;
    private final Output output;

    StateHolder(final State state, final Output output) {
        if (state == null || output == null) {
            throw new IllegalArgumentException("State and output must not be null!");
        }
        this.state = state;
        this.output = output;
    }

    State getState() {
        return state;
    }

    Output getOutput() {
        return output;
    }

    public String toString() {
        return "@" + state.name() + "|" + output.name();
    }

}
