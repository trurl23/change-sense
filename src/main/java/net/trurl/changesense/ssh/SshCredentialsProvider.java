package net.trurl.changesense.ssh;

import net.trurl.changesense.ssh.impl.SshSessionFactory;
import org.eclipse.jgit.errors.UnsupportedCredentialItem;
import org.eclipse.jgit.transport.CredentialItem;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.URIish;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.stream.Stream;

/**
 * Base class for SSH credentials provider. Override {@link #handleCredentialItem(URIish, CredentialItem)} to handle
 * incoming credential item events (e.g. to supply passwords or accept host keys). The keyfile (private key) here is
 * added to the identities used by {@link com.jcraft.jsch.JSch} to establish an SSH connection.
 * This happens in {@link SshSessionFactory}.
 */
public class SshCredentialsProvider extends CredentialsProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(SshCredentialsProvider.class);

    private final String remoteUrl;

    private final String keyFile;

    public SshCredentialsProvider(final String remoteUrl,
                                  final String keyFile) {
        this.keyFile = keyFile;
        this.remoteUrl = remoteUrl;
        if (remoteUrl == null) {
            throw new IllegalArgumentException("Remote URL must not be null!");
        }
    }

    public String getKeyFile() {
        return keyFile;
    }

    @Override
    public boolean isInteractive() {
        return false;
    }

    @Override
    public boolean supports(final CredentialItem... items) {
        return true;
    }

    protected boolean handleCredentialItem(final URIish uri, final CredentialItem credentialItem) {
        LOGGER.info("{} {}", uri, credentialItem.getClass().getSimpleName());
        return false;
    }

    @Override
    public boolean get(final URIish uri, final CredentialItem... items) throws UnsupportedCredentialItem {
        Stream.of(items).forEach(item -> handleCredentialItem(uri, item));
        return false;
    }
}
