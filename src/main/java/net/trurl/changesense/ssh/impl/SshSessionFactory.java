package net.trurl.changesense.ssh.impl;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import net.trurl.changesense.ssh.SshCredentialsProvider;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig;
import org.eclipse.jgit.util.FS;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Session factory that sets up the identity for the underlying SSH connection by using an SSH credentials provider.
 * Does nothing when the credentials provider is not an {@link SshCredentialsProvider}.
 */
public class SshSessionFactory extends JschConfigSessionFactory {

    private final Logger LOGGER = LoggerFactory.getLogger(SshSessionFactory.class);

    private final CredentialsProvider credentialsProvider;

    public SshSessionFactory(final CredentialsProvider credentialsProvider) {
        this.credentialsProvider = credentialsProvider;
    }

    @Override
    protected void configure(final OpenSshConfig.Host hc, final Session session) {

    }

    @Override
    protected JSch createDefaultJSch(final FS fs) throws JSchException {
        final JSch defaultJSch = super.createDefaultJSch(fs);
        addIdentities(defaultJSch);
        return defaultJSch;
    }

    private void addIdentities(final JSch defaultJSch) throws JSchException {
        if (credentialsProvider instanceof SshCredentialsProvider) {
            final SshCredentialsProvider sshCredentialsProvider = (SshCredentialsProvider) credentialsProvider;
            if (sshCredentialsProvider.getKeyFile() != null) {
                defaultJSch.addIdentity(sshCredentialsProvider.getKeyFile());
            } else {
                LOGGER.warn("No key file specified!");
            }
        }
    }
}
