package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.util.regex.Pattern;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class GitRemoteRepositoryServiceIntegrationTest extends AbstractRepositoryServiceIntegrationTest {
    @Test
    public void createRepositoryFromScratchShouldCreateDirectoryAndCloneRepository() throws IOException {
        final File tempDir = createTempDir();
        createRemoteGitRepo(tempDir);
        assertThat(tempDir.exists(), is(true));
        createRemoteGitRepo(tempDir)
                .calcChangedFiles("rev-b^", "rev-c", Pattern.compile("^.*\\.properties$"));
    }

    @Test
    public void tryResolveReferenceShouldResolveMaster() throws IOException {
        final File tempDir = createTempDir();
        final ChangeRepository changeRepository = createRemoteGitRepo(tempDir);
        assertThat(changeRepository.tryResolveReference("master"), is(true));
    }

    @Test
    public void tryResolveReferenceShouldNotResolveUnknown() throws IOException {
        final File tempDir = createTempDir();
        final ChangeRepository changeRepository = createRemoteGitRepo(tempDir);
        assertThat(changeRepository.tryResolveReference("someGarbage"), is(false));
    }

}