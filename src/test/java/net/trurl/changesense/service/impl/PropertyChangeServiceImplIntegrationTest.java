package net.trurl.changesense.service.impl;

import net.trurl.changesense.model.ChangeRepository;
import net.trurl.changesense.model.PropertiesFileDifference;
import net.trurl.changesense.model.Property;
import net.trurl.changesense.model.PropertyDifference;
import net.trurl.changesense.model.impl.DefaultChangeSenseConfiguration;
import net.trurl.changesense.service.PropertyChangeService;
import net.trurl.changesense.testutil.TestConfiguration;
import org.apache.commons.io.FilenameUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(MockitoJUnitRunner.class)
public class PropertyChangeServiceImplIntegrationTest extends AbstractRepositoryServiceIntegrationTest {

    private static final String EXPECTED_DOCUMENTATION = "#%n" +
            "# Some documentation above%n" +
            "#%n";
    private static final String REMOTE_URL = TestConfiguration.getGitRepoUrl();
    private static final String ROOT_PROPERTIES = "root.properties";
    private static final String SUBDIR_B_SUBDIR_PROPERTIES = "subdirB/subdir.properties";
    private static final String SUBDIR_A_SUBDIR_PROPERTIES = "subdirA/subdir.properties";
    private static final String PROP_REMOVED_IN_REVISION_B = "prop.removed-in-revision-b";
    private static final String TRUE = "true";
    private static final String PROP_ADDED_IN_REVISION_B = "prop.added-in-revision-b";
    private static final String PROP_ADDED = "prop.added";
    private static final String PROP_ADDED_VALUE = "I care deeply.";
    private static final String PROP_REMOVED = "prop.removed";
    private static final String PROP_REMOVED_VALUE = "I don't care";
    private static final String PROP_NOCHANGE = "prop.nochange";
    private static final String PROP_NOCHANGE_VALUE = "7654321";

    /**
     * Make sure that getting property differences between revisions works.
     */
    @Test
    public void getPropertyDifferencesBetweenRevisionsShouldWork() throws IOException {
        final ChangeRepository changeRepository = createRemoteGitRepo(createTempDir());
        final PropertyChangeService propertyChangeService = new PropertyChangeServiceImpl(
                new DefaultChangeSenseConfiguration());
        final List<PropertiesFileDifference> differenceList = propertyChangeService.getPropertyDifferences(
                changeRepository, "rev-e^", "rev-f", "", "some/sub/dir");
        assertThat(differenceList.size(), is(1));

        final PropertiesFileDifference difference = differenceList.get(0);
        assertThat(difference.getFile().getFilename(), is("top-level.properties"));
        assertThat(difference.getProperties().size(), is(2));
        assertThat(difference.getProperties(PropertyDifference.DifferenceType.REMOVED).size(), is(1));

        final PropertyDifference propertyDifference = difference.getProperties(
                PropertyDifference.DifferenceType.REMOVED).get(0);
        assertThat(propertyDifference.getDifferenceType(), is(PropertyDifference.DifferenceType.REMOVED));

        final Property property = propertyDifference.getBefore();
        assertThat(property.getName(), is("some.test.property"));
        //noinspection RedundantStringFormatCall
        assertThat(property.getDocumentation(), is(String.format(EXPECTED_DOCUMENTATION)));
        assertThat(property.getValue(), is("failure"));
        assertThat(propertyDifference.getAfter(), nullValue());
    }

    @Test
    public void getPropertyDifferencesShouldKeepOrder() throws IOException {
        final ChangeRepository changeRepository = createRemoteGitRepo(createTempDir());
        final PropertyChangeService propertyChangeService = new PropertyChangeServiceImpl(new DefaultChangeSenseConfiguration());
        final List<PropertiesFileDifference> differenceList = propertyChangeService.getPropertyDifferences(
                changeRepository, "rev-f^", "release/1.0", "", "some/sub/dir");
        final PropertiesFileDifference topLevel = differenceList.stream()
                .filter(diff -> FilenameUtils.getBaseName(diff.getFile().getFilename()).equals("top-level"))
                .findFirst()
                .orElseThrow(IllegalStateException::new);
        assertThat(topLevel.getProperties().size(), is(2));
        assertThat(topLevel.getProperties().get(0).getAfter().getName(), is("added.property"));
        assertThat(topLevel.getProperties().get(1).getAfter().getName(), is("new.property1"));
    }

    @Test
    public void getPropertyDifferencesShouldWorkWithBranches() throws IOException {
        final ChangeRepository changeRepository = createRemoteGitRepo(createTempDir());
        final PropertyChangeService propertyChangeService = new PropertyChangeServiceImpl(new DefaultChangeSenseConfiguration());
        final List<PropertiesFileDifference> differenceList = propertyChangeService.getPropertyDifferences(
                changeRepository, "rev-f^", "release/1.0", "", "some/sub/dir");
        assertThat(differenceList.size(), is(2));

        final PropertiesFileDifference topLevelProps = differenceList.get(1);
        assertThat(topLevelProps.getFile().getFilename(), is("top-level.properties"));
        assertThat(topLevelProps.getProperties().size(), is(2));
        assertThat(topLevelProps.getProperties(PropertyDifference.DifferenceType.ADDED).size(), is(1));

        final PropertyDifference propertyDifference = topLevelProps.getProperties(
                PropertyDifference.DifferenceType.ADDED).get(0);
        assertThat(propertyDifference.getDifferenceType(), is(PropertyDifference.DifferenceType.ADDED));

        final Property property = propertyDifference.getAfter();
        assertThat(property.getName(), is("added.property"));

        final PropertiesFileDifference anotherProperties = differenceList.get(0);
        assertThat(anotherProperties.getFile().getFilename(), is("some/sub/dir/another-props.properties"));
        assertThat(anotherProperties.getProperties().size(), is(2));
        assertThat(anotherProperties.getProperties(PropertyDifference.DifferenceType.REMOVED).size(), is(1));

        final PropertyDifference removedPropertyDifference = anotherProperties.getProperties(
                PropertyDifference.DifferenceType.REMOVED).get(0);
        assertThat(removedPropertyDifference.getDifferenceType(), is(PropertyDifference.DifferenceType.REMOVED));

        final Property removedProperty = removedPropertyDifference.getBefore();
        assertThat(removedProperty.getName(), is("changy.change"));
    }

    @Test
    public void getPropertyDifferencesShouldWorkWithFilesystem() throws URISyntaxException {
        final ChangeRepository changeRepository = createFilesystemRepository();

        final File revA = new File(PropertyChangeServiceImplIntegrationTest.class.getResource("/revA").toURI());
        final File revB = new File(PropertyChangeServiceImplIntegrationTest.class.getResource("/revB").toURI());

        final PropertyChangeService propertyChangeService = new PropertyChangeServiceImpl(new DefaultChangeSenseConfiguration());
        final List<PropertiesFileDifference> differenceList = propertyChangeService.getPropertyDifferences(
                changeRepository, revA.getAbsolutePath(), revB.getAbsolutePath(), "", "subdirA", "subdirB", "static");

        checkDifferences(differenceList);
    }

    @Test
    public void getPropertyDifferencesShouldWorkWithArchives() throws URISyntaxException {
        final ChangeRepository changeRepository = createArchiveRepository();

        final File revA = new File(PropertyChangeServiceImplIntegrationTest.class.getResource("/revA.zip").toURI());
        final File revB = new File(PropertyChangeServiceImplIntegrationTest.class.getResource("/revB.zip").toURI());

        final PropertyChangeService propertyChangeService = new PropertyChangeServiceImpl(new DefaultChangeSenseConfiguration());
        final List<PropertiesFileDifference> differenceList = propertyChangeService.getPropertyDifferences(
                changeRepository, revA.getAbsolutePath(), revB.getAbsolutePath(), "", "subdirA", "subdirB", "static");

        checkDifferences(differenceList);
    }

    private void checkDifferences(final List<PropertiesFileDifference> differenceList) {
        assertThat(differenceList.size(), is(3));

        final Map<String, PropertiesFileDifference> propertiesFileDifferences =
                differenceList.stream()
                        .collect(Collectors.toMap(pfd -> pfd.getFile().getFilename(), Function.identity()));
        assertThat(propertiesFileDifferences.containsKey(SUBDIR_A_SUBDIR_PROPERTIES), is(true));

        final PropertiesFileDifference rootProperties = propertiesFileDifferences.get(ROOT_PROPERTIES);
        assertThat(rootProperties, notNullValue());
        assertThat(rootProperties.getProperties().size(), is(3));

        assertThat(rootProperties.getProperties()
                .stream()
                .map(PropertyDifference::getDifferenceType)
                .collect(Collectors.toList()), contains(PropertyDifference.DifferenceType.ADDED,
                PropertyDifference.DifferenceType.REMOVED, PropertyDifference.DifferenceType.UNCHANGED));
        checkDifferences(rootProperties, 0, PropertyDifference.DifferenceType.ADDED,
                PROP_ADDED, PROP_ADDED_VALUE, 3);
        checkDifferences(rootProperties, 1, PropertyDifference.DifferenceType.REMOVED,
                PROP_REMOVED, PROP_REMOVED_VALUE, 3);
        checkDifferences(rootProperties, 2, PropertyDifference.DifferenceType.UNCHANGED,
                PROP_NOCHANGE, PROP_NOCHANGE_VALUE, 3);

        final PropertiesFileDifference subdirAProperties = propertiesFileDifferences.get(SUBDIR_A_SUBDIR_PROPERTIES);
        checkDifferences(subdirAProperties, 0, PropertyDifference.DifferenceType.REMOVED,
                PROP_REMOVED_IN_REVISION_B, TRUE, 1);

        final PropertiesFileDifference subdirBProperties = propertiesFileDifferences.get(SUBDIR_B_SUBDIR_PROPERTIES);
        checkDifferences(subdirBProperties, 0, PropertyDifference.DifferenceType.ADDED,
                PROP_ADDED_IN_REVISION_B, TRUE, 1);
    }

    private void checkDifferences(final PropertiesFileDifference propertyFileDifference,
                                  final int index,
                                  final PropertyDifference.DifferenceType expectedDifferenceType,
                                  final String expectedPropName,
                                  final String expectedPropValue,
                                  final int expectedSize) {
        assertThat(propertyFileDifference, notNullValue());
        assertThat(propertyFileDifference.getProperties(), hasSize(expectedSize));

        final PropertyDifference diff = propertyFileDifference.getProperties().get(index);
        assertThat(diff.getDifferenceType(), is(expectedDifferenceType));
        final Property prop;
        if (expectedDifferenceType == PropertyDifference.DifferenceType.REMOVED) {
            prop = diff.getBefore();
            assertThat(diff.getAfter(), nullValue());
        } else {
            prop = diff.getAfter();
            if (expectedDifferenceType == PropertyDifference.DifferenceType.ADDED) {
                assertThat(diff.getBefore(), nullValue());
            } else if (expectedDifferenceType == PropertyDifference.DifferenceType.UNCHANGED) {
                assertThat(diff.getBefore(), notNullValue());
                assertThat(diff.getBefore(), is(diff.getAfter()));
            }
        }
        assertThat(prop, notNullValue());
        assertThat(prop.getName(), is(expectedPropName));
        assertThat(prop.getValue(), is(expectedPropValue));
    }

}