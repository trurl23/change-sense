package net.trurl.changesense.testutil;

import net.trurl.changesense.ssh.SshCredentialsProvider;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jgit.transport.CredentialsProvider;
import org.eclipse.jgit.transport.UsernamePasswordCredentialsProvider;

import java.io.IOException;
import java.util.Properties;

/**
 * Default integration test configuration, reads properties file.
 */
public abstract class TestConfiguration {

    /**
     * Test configuration properties in the class-path.
     */
    private static final String TESTCONFIG_PROPERTIES = "/testconfig.properties";
    /**
     * Git repo URL configuration p roperty.
     */
    private static final String GIT_REPO_URL = "git.repo.url";
    private static final String GIT_USERNAME = "git.username";
    private static final String GIT_PASSWORD = "git.password";
    private static final String GIT_KEYFILE = "git.keyfile";
    private static final String GIT_AUTH_TYPE = "git.auth.type";

    public enum AuthType {
        NONE,
        USERNAME_PASSWORD,
        KEY;

        public static AuthType from(final String authType) {
            if (StringUtils.isBlank(authType)) {
                return null;
            }
            final String upperAuthType = StringUtils.upperCase(authType);
            for (final AuthType candidate : values()) {
                if (StringUtils.equals(upperAuthType, candidate.name())) {
                    return candidate;
                }
            }
            throw new IllegalArgumentException(String.format("Unsupported auth type %s!", upperAuthType));
        }
    }

    /**
     * Stores test configuration properties.
     */
    private static final Properties properties = new Properties();

    /*
     * Read {@value #TESTCONFIG_PROPERTIES} from class-path.
     */
    static {
        try {
            properties.load(TestConfiguration.class.getResourceAsStream(TESTCONFIG_PROPERTIES));
        } catch (final IOException e) {
            throw new IllegalStateException("Error loading integration test configuration!", e);
        }
    }

    private TestConfiguration() {
    }

    /**
     * Get property value from system properties or from the test-configuration properties if no system property is
     * set.
     *
     * @param propertyName Name of property.
     * @return Property value.
     */
    private static String getSystemPropertyOrProperty(final String propertyName) {
        final String sysProp = System.getProperty(propertyName);
        if (StringUtils.isBlank(sysProp)) {
            return properties.getProperty(propertyName);
        }
        return sysProp;
    }

    public static String getGitRepoUrl() {
        return getSystemPropertyOrProperty(GIT_REPO_URL);
    }

    private static AuthType getGitAuthType() {
        return AuthType.from(getSystemPropertyOrProperty(GIT_AUTH_TYPE));
    }

    private static String getGitUsername() {
        return getSystemPropertyOrProperty(GIT_USERNAME);
    }

    private static String getGitPassword() {
        return getSystemPropertyOrProperty(GIT_PASSWORD);
    }

    private static String getKeyfile() {
        return getSystemPropertyOrProperty(GIT_KEYFILE);
    }

    public static CredentialsProvider getCredentialsProvider(final String remoteUrl) {
        if (StringUtils.isBlank(remoteUrl)) {
            throw new IllegalArgumentException("Remote URL must not be blank!");
        }
        final AuthType authType = getGitAuthType();
        if (authType == null || authType == AuthType.NONE) {
            return new UsernamePasswordCredentialsProvider("", "");
        }
        switch (authType) {
            case USERNAME_PASSWORD:
                return new UsernamePasswordCredentialsProvider(getGitUsername(), getGitPassword());
            case KEY:
                return new SshCredentialsProvider(remoteUrl, getKeyfile());
            default:
                throw new IllegalStateException(String.format("Unsupported auth type '%s'!", authType));
        }
    }
}
